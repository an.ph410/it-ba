# PHÂN TÍCH HỆ THỐNG

_Tham khảo: https://thinhnotes.com/chuyen-nghe-ba/viet-dac-ta-use-case-sao-don-gian-nhung-hieu-qua/_
### I/ Chuyển đổi như thế nào?
```
- Phân tích hệ thống là điểm giao thoa giữa phân tích nghiệp vụ và phân tích hệ thống.
- Có rất nhiều cách chuyển đổi, VD:
- Use case → Flow → State → Database → Use case
```

### II/ Use Case
```
- Use case hoạt động được thực hiện trên System
- Nhiều use cases gộp lại được dùng để thể hiện các hoạt động (activity) để hoàn thành một mục tiêu nào đó.
  Mục tiêu có thể là process, flow, funtion
```

![Non-Functional_Requirement!](../Non-Functional_Requirement/NFR.png "Non-Functional_Requirement")

__MÔ TẢ USE CASE__

|Tên|Mô tả|Note|
|---|-----|----|
|Use Case ID|Mã số|Summary|
|Use Case Name|Tên use case|Summary|
|Use Case Description|Tóm gọn nhanh sự tương tác được thể hiện trong Use Case là gì.|chỉ cần mô tả ngắn gọn theo cú pháp của User Story: Là “Actor”, tui muốn làm “Use Case Name”, để đạt được “mục đích – lý do” gì đó. Đẹp là không quá 3 dòng cho phần tóm gọn Use Case này. Summary|
|Actor|Những đối tượng thực hiện sự tương tác trong Use Case.|Summary|
|Priority|Mức độ ưu tiên của Use Case so với các Use Case còn lại trong dự án.|Summary|
Trigger|Điều kiện kích hoạt Use Case xảy ra.|Summary|
|Tiền điều kiện - Precondition|Điều kiện cần để Use Case thực hiện thành công.|Summary|
|Hậu điều kiện - Postcondition|Những thứ sẽ xuất hiện sau khi Use Case được thực hiện thành công.|Summary|
|Luồng cơ sở - Basic path|luồng tương tác CHÍNH giữa các Actor và System để Use Case thực hiện thành công.|Flow|
|Luồng thay thế - Alternative paths|luồng tương tác THAY THẾ giữa các Actor và System để Use Case thực hiện thành công.|Flow|
|Luồng ngoại lệ - Exception paths|luồng tương tác NGOẠI LỆ giữa các Actor và System mà Use Case thực hiện thất bại.|Flow|
|Quy định nghiệp vụ - Business rule|các quy định về mặt Business mà hệ thống bắt buộc phải nghe theo, làm theo.| Additional Information. là các quy định, hoặc các policy của khách hàng. Có sao ghi vậy. Vì đây là giai đoạn tài liệu hóa yêu cầu thông qua Use Case, nên anh em cứ liệt kê hết các Business Rule có liên quan trong này nhé.|
|Yêu cầu phi chức năng - Non-functional Requirement|Vì Use Case chỉ dùng để thể hiện Functional Requirement, nên anh em phải bổ sung các yêu cầu về Non-Functional ở đây luôn.|Additional Information|

### III/ Activity Diagram
![diagram_activity!](../Non-Functional_Requirement/diagram_activity.PNG "diagram_activity")

### IV/ State Diagram

```
    - Sự biến chuyển trong quá trình hoạt động, vận hành của system
    - Sử dụng trong các tình huống: hiển thị, update status, trao đổi thông tin
    - Thể hiện tương tự các flow diagram, process
```

### V/ Sequence Diagram
```
    - Thể hiện sự tương tác trong system hoặc giữa các system với nhau
    - Thường dùng trong các trường hợp như kết hợp chức năng với nhau, tích hợp hệ thống, kết nối API
```

### VI/ Function & Feature

| |Function|Feature|
|-----|--------|-------|
| |What system do?|Tool to satisfy function|
| | 1 function - many features | 1 feature - many functions|
|Definition|A goal or objecttive accomplished by a product or service| A tool that helps to achieve functions|
|Examples|Tasks, activities, calculations, outputs|doors, buttons, menus, robotic arms|

__Trình bày funtion__
- Tổng quan với Business Function Diagram: haraki chart
- Phương pháp

|STT|Name|
|---|----|
|1|Xác định thực thể|
|2|Main Function|
|3|Sub Function|