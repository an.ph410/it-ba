## I/  Phương pháp làm Dự án và Làm việc
1. Các phương pháp phân tích

|STT|Mô tả|
|:---:|:----:|
|1|Requirement → User Story → Business Process → Use case → Function/Feature|
|2|Requirement → Use Case → Function/Feature → Process/Flow|
|3|Requirement → Use Story → Function/Feature → Process/Flow|

2. Hệ thống tài liệu
```BRD/URD → SRS → Testcase```

|STT|Name|T/dung|
|---|----|------|
|1|BRD/URD|Business/ User Requirement Documents|
|2|SRS|System Requirement Specification|
|3|Testcase|Tài liệu kiểm thử

__CÁCH LÀM TÀI LIỆU BRD/URD/SRS__

*__Cấu trúc chung:__*

- Giới thiệu
- Tổng quan yêu cầu
- Yêu cầu phi chức năng
- Mô tả chi tiết
- Phụ lục

__CÁCH LÀM TEST CASE__

*__Cấu trúc chung:__*

- Kịch bản
- Kết quả mong đợi
- Kết quả thực tế
- Pass/Fail
- Ghi chú

## II/Một số nguyên tắc làm việc

- Lấy yêu cầu người dùng làm gốc
- Kết hợp Top-down và bottom-up
- Không cần lúc nào cũng phải đúng chuẩn

**_→ Quan trọng là hoàn thành sản phẩm_**

## III/ Kết luận
1. IT BA là gì?
- Là BA làm việc trong domain IT, trong môi trường công nghệ, kỹ thuật số
- Chuyển đổi sản phẩm, nhu cầu nghiệp vụ thành sản phẩm số.
2. Một số Tips
- Nắm rõ khái niệm, định nghĩa
- Hiểu rõ công cụ
- Linh hoạt sử dụng
- Phân biệt rõ business và system
- Sản phẩm số cần đi kèm quy trình số
- Không có đường tắt
- Rộng phải đi với SÂU
- Vô chiêu thắng hữu chiêu

![TIPS!](tipsBA.png "TIPS") 
