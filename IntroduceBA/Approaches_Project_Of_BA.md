PHƯƠNG THỨC TIẾP CẬN DỰ ÁN CỦA ITBA

- Life Cycle Of Software Development: 

- Flow: Lên ý tưởng → Thu thập thông tin, yêu cầu từ các bên liên quan → Phân tích yêu cầu → Thiết kế sản phẩm → Phát triển (coding) → Có sản phẩm đầu ra → Testing → Triển khai → Maintenance (complain, feedback, fix...) → quay lại lifecycle

- List các phương thức tiếp cận dự án: Waterfall, Prototype, Spiral, Incremental, Agile,...

    1. Waterfall: tất cả công việc được thực hiện từ trên xuống dưới, đòi hỏi sự liên tục phát triển. Có chút cứng nhắc.
    2. Agile: liên tục phân tích, liên tục phát triển, linh hoạt, đáp ứng được sự thay đổi liên tục
    3. Trong Agile có mô hình (framework) Scrum:
        - Là một mô hình phổ biến trong quy trình phát triển phần mềm outsource
        - Gồm 3 thành phần chính:
            + Product Owner: roles giống BA, thu thập thông tin khách hàng, thị trường → đưa ra tính năng, đầu mục công việc, đưa ra các mong muốn gọi là user story → Product Backlog: là một đơn vị tính công việc trong Scrum.
            + The Team → Sprint Planning Meeting: cấu phần hoàn thành dự án Dev, QC, BA,...
            + Scrum Master: giống quản gia, triển khai backlog cho The Team, run daily scrum, review print, finished work, sprint retrospective...

- Tại sao Agile - Scrum dễ thất bại?
    + Thiếu sự công bằng và minh bạch
    + Khâu planning gặp trục trặc → team phải OT, đi năn nỉ khách hàng
    + Chưa hiểu rõ mô hình hầu hết là người Việt Nam
    + Nhất hậu duệ, nhì quan hệ, tam tiền tệ

- Các giai đoạn làm việc theo thứ tự:
    1. Business Analysis: phân tích nghiệp vụ
        - Mô tả thế giới thực
        - Nói về quy trình person to person, persion to device, device to device
        - Yêu cầu chức năng và phi chức năng
        - Cần đảm bảo value, đảm bảo journey hoàn chỉnh
    2. Logic Analysis: 
        - Sự kết hợp: sự phối hợp giữa người, giữa máy → logic rất quan trọng
        - Đa chiều: overview nhiều thứ, hợp lý ?
        - Yếu tố tác động: pháp luật, quy định chính sách, môi trường
        - Trải nghiệm thực tế
    3. System Analysis: chuyển đổi từ nghiệp vụ lên hệ thống thông qua logic
        - Quy luật: Input, Process, Output
        - Thế giới thực sẽ đóng vai trò "Tác nhân"
    4. UI/UX: giao diện người dùng interface → trải nghiệm → đồng bộ luồng system
