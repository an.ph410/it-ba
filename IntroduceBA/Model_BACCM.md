MÔ HÌNH BACCM - BUSINESS ANALYSIS CORE CONCEPT MODEL

- Là một mô hình nền tảng cần thiết!

- Mô hình BACCM có 6 nhánh, được dựa trên ngôi sao David.

- 6 Nhánh của BACCM:
    1. Change: Thay đổi điều gì đó để Giải quyết vấn đề, tận dụng cơ hội
    2. Solution: Giải pháp cụ thể tạo nên sự tay đổi
    3. Contexts: Hoàn cảnh, môi trường. Cái gì bị tác động, bị ảnh hưởng?
    4. Value: Giá trị mang lại
    5. Stakeholders: Các bên liên quan
    6. Needs: Vấn đề và cơ hội

KHƠI GỢI YÊU CẦU VÀ THU THẬP THÔNG TIN

- Xác định hoàn cảnh
- Xác định các yếu tố liên quan
- Xác định vấn đề, cơ hội

PHƯƠNG PHÁP

- Phỏng vấn
- Đọc và phân tích tài liệu
- Khảo sát
- Quan sát
- Phân tích UI/UX

CÁC BƯỚC THỰC HIỆN

* Không nên làm theo flow sau :) Đừng lên list :) Làm ơn
    - Lên list câu hỏi
    - Gặp khách hàng/stakeholder
    - Hỏi và yêu cầu KH/stakeholder
    - Ghi chép
    - Mang về phân tích

* Hãy làm như sau:
    - Bước 1. Phân tích BACCM 
    - Bước 2. Xác định cụ thể Needs của Stakeholder → Sử dụng mô hình phân cấp để trình bày
    - Bước 3. Đưa ra giải pháp phù hợp với Needs → Kết hợp user story
    - Bước 4. Chuẩn bị bài thuyết trình và list các câu hỏi cần làm rõ
    - Bước 5. Gặp khách hàng và trình bày phân tích, giải pháp
    - Bước 6. Trao đổi với KH để xác nhận hoặc điều chỉnh bổ sung, làm rõ nhu cầu
