1. Khái niệm
- Hiểu đơn giản, IT BA là người phiên dịch thông tin giữa IT và Stake-holder trong việc phát triển phần mềm.
- Là một hoạt động tạo ra sự thay đổi của tổ chức bằng cách định nghĩa những nhu cầu, vấn đề, cơ hội, ý tưởng → từ đó đưa ra các giải pháp đem đến giá trị cho stack-holder.
- Là 1 BA làm cho domain IT.
- Là 1 bộ skill set để làm việc mà thôi.

2. BA vs PO & PM
- Tất cả các roles này đều có thể gọi là BA. Vì tất cả roles này đều yêu cầu tất cả kỹ năng, công cụ của BA. Lúc này BA chỉ là 1 skill set của những roles trên. 
- PO không phải manager level, nó là 1 roles khi chúng ta áp dụng mô hình srum, agile trong quản trị dự án.
- Nhận định ở Việt Nam "PM là sếp, PO là quản lý" → không đúng hoàn toàn. 
- PM là người managed 1 cái gì đó, quản lý sản phẩm. PM có phải sếp hay không ? thì phụ thuộc vào sơ đồ tổ chức.
- CIO/CTO: quản lý
