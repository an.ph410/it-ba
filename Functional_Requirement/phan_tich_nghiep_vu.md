# PHÂN TÍCH NGHIỆP VỤ

### I. Công việc cần làm 
![Công việc cần làm!](to_do_list.PNG "Công việc cần làm") 

### II. Phân tích yêu cầu chức năng - Functional Requirement

1. Làm việc gì cũng cần làm theo phương pháp

![FR1!](FunctionalRequirement_p1.png "Fuctional_Requirement_P1") 

2. Làm gì cũng cần thấu hiểu và nắm rõ bản chất

![FR2!](FR_p2.png "Fuctional_Requirement_P2")

3. Không có gì là tốt nhất, chỉ có hợp lý nhất

![FR1!](FR_P3.png "Fuctional_Requirement_P3")


### III. Thực hành
1. Tình huống

``` Hãy mô tả quy trình nghiệp vụ tính tiền tại siêu thị. ```

|Cách 1| Cách 2|
|:------:|:-------:|
|Mua sắm → Chọn quầy tính tiền → Xếp hàng → Bỏ hàng lên quầy khi tới lượt → Nhập liệu hàng hóa → Kiểm tra hàng hóa → Thanh toán (cash/thẻ/ví) → kiểm tra hàng, hóa đơn và ra về|Sau khi thực hiện chọn hàng hóa xong, KH đến khu vực tính tiền và chọn quầy. Trong trường hợp đang có khách khách, KH thực hiện xếp hàng chờ đến lượt. Khi tới kuotwj, KH thucjwh iện bỏ hàng hóa lên quầy tính và NV thu ngân nhập liệu hàng hóa. Sau khi nhập liệu xong, NV thu ngân nhờ KH kiểm tra lại và yêu cầu thanh toán, NV thu ngân thu tiền mặt hoặc xử lý thanh toán nếu KH dùng thẻ hay ví. Sau khi thanh toán xong, NV thu ngân gửi Receipt cho KH và ra về.|
|SAI LẦM. Dễ gặp ở BA mới|NÊN LÀM|

-----------

``` Mục tiêu đạt được ```

|Mục tiêu|Note|
|:---------:|:---------:|
|Xác định stakeholder||
|Xác định bắt đầu và kết thúc||
|Xác định Milestone|mua sắm, xếp hàng, nhập liệu, kiểm tra, thanh toán|
|Mô tả chi tiết activity từng milistone||
|Nhìn lại||

---------------
``` Chuyển thể hình ảnh ```

![ThucHanhSieuThi!](PayOrderProgress.png "Tính tiền siêu thị") 

2. Tình huống

``` 
- Một trung tâm Anh ngữ dang trong quá trình cải thiện hoạt động kinh doanh, thương mại ra bên ngoài với khách hàng.
- Họ mong muốn xây dựng một mobile app để giúp cho học viên, phụ huynh, khách hàng có thể sử dụng các tiện ích của trung tâm, 
giao tiếp với trung tâm.
```
---

* __Xác định tổng quan theo mô hình BACCM__


|Nhánh|Mô tả|Cốt lõi|
|-----|------|-----|
|Solution|Chuyển đổi số quy trình học tập, xử lý giáo vụ. Bổ sung mobile app/ landing page. Phát triển các tiện ích phù hợp|Thay đổi điều gì đó để Giải quyết vấn đề, tận dụng cơ hội|
|Contexts|Tuyển sinh, dạy học, giáo vụ, tin tức/ thông tin.|Giải pháp cụ thể tạo nên sự tay đổi
|Value|Gia tăng khả năng tuyển sinh. Phụ huynh theo sát công việc học tập. Tăng tương tác của học viên, giảng viên|Hoàn cảnh, môi trường. Cái gì bị tác động, bị ảnh hưởng?
|Stakeholders| Học viên, giáo viên, phụ huynh, nhân viên, lãnh đạo.|Giá trị mang lại
|Needs|Thiếu công cụ theo dõi, tương tác. Việc theo dõi học tập của phụ huynh còn nhiều trở ngại. Bổ sinh công cụ về tuyển sinh, tin tức.|Các bên liên quan
|Change|Tổng hợp lại công cụ về học tập, giáo vụ, giao tiếp. Đa dạng hóa phương thức tuyển sinh|Vấn đề và cơ hội

* __Mô tả Business Process__

![TrungTamAnhNgu!](TrungTamAnhNguc.png "Trung tâm anh ngữ") 

* __Sau khi có mô hình Business Process → Phân tích Logic__

```angular2html
    - Các User Story có phù hợp
    - Liên kết giữa US và BP có phù hợp hay chưa
    - Cần bổ sung các sự kiện, liên kết như thế nào để gia tăng trải nghiệm người dùng
    - Nguy cơ rủi ro có thế xảy ra và phương pháp khắc phục
    - Thống nhất lại nghiệp vụ và lên tài liệu BRD

```

* __Tài liệu BRD__

```angular2html
    - Tổng quan dự án (tình huống, vấn đề, hiện trạng)
    - Mục đích dự án
    - Yêu cầu người dùng (User Story)
    - Quy trình nghiệp vụ (Business Process)
    - Yêu cầu phi chức năng

```